## Storybook Guidelines

### Code Context:
The below code snippet is a sample storybook file for a component named `Button`. The component is exported from `Button.tsx` file.
<img src='https://i.imgur.com/dZEaQ62.png' />

---

### Guidelines
- Recommended to create separate storybook files for multiple components that are exported from the same component file; eg:
	- if `SideMenu.tsx` contains two exports, namely: `<SideMenu/>` and `<SideMenuItem/>`, then please create two separate storybook files: `SideMenu.stories.tsx` and `SideMenuItem.stories.tsx`.
	- `SideMenu.stories.tsx` should contain stories **only** for `<SideMenu/>` and `SideMenuItem.stories.tsx` should contain stories **only** for `<SideMenuItem/>`.
	- Export the storybook component without any parent / wrapper component.
    	- Do:
			```
			const Template: ComponentStory<typeof SideMenu> = (args: ISideMenuProps) => {
				return (
					<SideMenu {...args} />
				);
			};
			```
        - Don't:
			```
			const Template: ComponentStory<typeof SideMenu> = (args: ISideMenuProps) => {
				return (
					<div style={{ display: 'flex', alignItems: 'center' }}>
						<SideMenu {...args} />
					</div>
				);
			};
			```
		- Why?
    		- On viewing the storybook component, the parent / wrapper component will be shown in the storybook component *code preview* making the code cluttered and difficult to view the component in isolation.
- The `children` should be passed via the `args` prop. (like all other prop except for reactive states).
    - Do:
		```
		const Template: ComponentStory<typeof SideMenu> = (args: ISideMenuProps) => {
			return (
				<SideMenu {...args} />
			);
		};
		const baseSideMenuProps: ISideMenuProps = {
			children: [
				<SideMenuItem label="SideMenuItem 1" />,
				<SideMenuItem label="SideMenuItem 2" />,
			],
		};
		export const Default = Template.bind({});
		Default.args = {
			...baseSideMenuProps,
			children: [ // override children if needed
				<SideMenuItem label="SideMenuItem 3" />,
				<SideMenuItem label="SideMenuItem 4" />,
			],
		};
		```
	- Don't:
		```
		const Template: ComponentStory<typeof SideMenu> = (args: ISideMenuProps) => {
			return (
				<SideMenu {...args}>
					<SideMenuItem>Item 1</SideMenuItem>
					<SideMenuItem>Item 2</SideMenuItem>
				</SideMenu>
			);
		};
		export const Default = Template.bind({});
		Default.args = {};
		```
	- Why?
    	- `args` already contain the `children` prop, which gets destructured and passed to the component. So, there is no need to pass the `children` prop again in the Template component.

---

### How To

- Use a reactive prop inside the storybook:
    - How:
		<img src='https://i.imgur.com/QDQ73FO.png' />
	- Why?
    	- the `useEffect` on ==line 5== ensures that whenever the prop on the Storybook's UI front is changed, the state inside the storybook component should also change.
    	
---

### FAQs:

<details>
<summary>What all aspects to cover in storybook's controls?</summary>

- All the props that are passed to the component should be covered in the controls.
- Except for ReactNode props.
</details>

<details>
<summary>Which controls to disable in storybook?</summary>	

- Any prop which accepts `typeof ReactNode` only.
    - Eg: `children`

- Why?
    - Storybook UI controls cannot take `ReactNode` from user input.
- How?
    - Code Example:
		```
		export default {
			title: "Components/SideMenu",
			component: SideMenu,
			argTypes: {
				children: {
					table: {
						disable: true,
					},
				},
			},
		} as ComponentMeta<typeof SideMenu>;
		```
	- A helper function named `storybookStoryDisable` is present inside utils folder which does the same, you can be used as:
		```
		import { storybookStoryDisable } from "utils/storybook/storyDisable";

		export default {
			title: "Components/SideMenu",
			component: SideMenu,
			argTypes: {
				children: storybookStoryDisable,
			},
		} as ComponentMeta<typeof SideMenu>;
		```
</details>
